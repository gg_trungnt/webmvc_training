﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Entities;
using WebMvc_Training.Repositories.UnitOfWork;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class BaseBusiness
    {
        protected DanangUniversityUnitOfWork UnitOfWork
        {
            get { return new DanangUniversityUnitOfWork(new DanangUniversityDbContext()); }
        }
    }
}
