﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class ClassBusiness : BaseBusiness, IClassBusiness
    {
        public List<ClassViewModel> GetAll()
        {
            var classes = UnitOfWork.ClassRepository.GetAll().ToList();
            return ClassMapper.ToViewModel(classes);
        }

        public List<ClassViewModel> FindBy(int id)
        {
            var classes = UnitOfWork.ClassRepository.FindBy(p => p.FacultyId == id).OrderBy(p => p.ClassName).ToList();
            return ClassMapper.ToViewModel(classes);
        }
        
        public ClassViewModel GetSingle(int id)
        {
            var singleClass = UnitOfWork.ClassRepository.FindBy(p => p.ClassId == id).SingleOrDefault();
            return ClassMapper.ToViewModel(singleClass);
        }

        public void Add(ClassViewModel viewModel)
        {
            var entity = ClassMapper.ToEntity(viewModel);
            UnitOfWork.ClassRepository.Add(entity);
        }

        public void Edit(ClassViewModel viewModel)
        {
            var entity = ClassMapper.ToEntity(viewModel);
            UnitOfWork.ClassRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            var entity = UnitOfWork.ClassRepository.FindBy(p => p.ClassId == id).SingleOrDefault();

            var students = entity.Students.Where(p => p.ClassId == id).ToList();

            foreach (var item in students)
            {
                var gradeStudents = item.GradeStudents.Where(p => p.StudentId == item.StudentId).ToList();

                foreach (var j in gradeStudents)
                {
                    UnitOfWork.GradeStudentRepository.Delete(p => p.StudentId == j.StudentId);
                }

                UnitOfWork.StudentRepository.Delete(p => p.StudentId == item.StudentId);
            }

            UnitOfWork.ClassRepository.Delete(p => p.ClassId == id);
        }
    }
}
