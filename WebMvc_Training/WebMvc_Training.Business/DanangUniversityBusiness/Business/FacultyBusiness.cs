﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class FacultyBusiness : BaseBusiness, IFacultyBusiness
    {
        public List<FacultyViewModel> GetAll()
        {
            var faculties = UnitOfWork.FacultyRepository.GetAll().OrderBy(p => p.FacultyName).ToList();
            return FacultyMapper.ToViewModel(faculties);
        }

        public List<FacultyViewModel> FindBy(int id)
        {
            throw new NotImplementedException();
        }
        
        public FacultyViewModel GetSingle(int id)
        {
            var faculty = UnitOfWork.FacultyRepository.FindBy(p => p.FacultyId == id).SingleOrDefault();
            return FacultyMapper.ToViewModel(faculty);
        }

        public void Add(FacultyViewModel viewModel)
        {
            var entity = FacultyMapper.ToEntity(viewModel);
            UnitOfWork.FacultyRepository.Add(entity);
            UnitOfWork.Commit();
        }

        public void Edit(FacultyViewModel viewModel)
        {
            var entity = FacultyMapper.ToEntity(viewModel);
            UnitOfWork.FacultyRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            //UnitOfWork.FacultyRepository.Delete(p => p.FacultyId == id);

            var entity = UnitOfWork.FacultyRepository.FindBy(p => p.FacultyId == id).SingleOrDefault();

            var classes = entity.Classes.Where(p => p.FacultyId == id).ToList();

            foreach (var item in classes)
            {
                var students = item.Students.Where(p => p.ClassId == item.ClassId).ToList();

                foreach (var j in students)
                {
                    var gradeStudents = j.GradeStudents.Where(p => p.StudentId == j.StudentId).ToList();

                    foreach (var x in gradeStudents)
                    {
                        UnitOfWork.GradeStudentRepository.Delete(p => p.StudentId == x.StudentId);
                    }

                    UnitOfWork.StudentRepository.Delete(p => p.StudentId == j.StudentId);
                }

                UnitOfWork.ClassRepository.Delete(p => p.ClassId == item.ClassId);
            }

            UnitOfWork.FacultyRepository.Delete(p => p.FacultyId == id);
        }
    }
}
