﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class GradeStudentBusiness : BaseBusiness, IGradeStudentBusiness
    {
        public List<GradeStudentViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<GradeStudentViewModel> FindBy(int id)
        {
            throw new NotImplementedException();
        }
        
        public List<GradeStudentViewModel> FindBy(int studentId, int semesterId)
        {
            var entities = UnitOfWork.GradeStudentRepository.FindBy(p => p.StudentId == studentId && p.SemesterId == semesterId).ToList();
            return GradeStudentMapper.ToViewModel(entities);
        }

        public GradeStudentViewModel GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(GradeStudentViewModel viewModel)
        {
            var entity = GradeStudentMapper.ToEntity(viewModel);
            UnitOfWork.GradeStudentRepository.Add(entity);
        }

        public void Edit(GradeStudentViewModel viewModel)
        {
            var entity = GradeStudentMapper.ToEntity(viewModel);
            UnitOfWork.GradeStudentRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            UnitOfWork.GradeStudentRepository.Delete(p => p.TeacherId == id);
        }

    }
}
