﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class SemesterBusiness : BaseBusiness, ISemesterBusiness
    {
        public List<SemesterViewModel> GetAll()
        {
            var entities = UnitOfWork.SemesterRepository.GetAll().ToList();
            return SemesterMapper.ToViewModel(entities);
        }

        public List<SemesterViewModel> FindBy(int id)
        {
            throw new NotImplementedException();
        }
        
        public SemesterViewModel GetSingle(int id)
        {
            var entity = UnitOfWork.SemesterRepository.FindBy(p => p.SemesterId == id).SingleOrDefault();
            return SemesterMapper.ToViewModel(entity);
        }

        public void Add(SemesterViewModel viewModel)
        {
            var entity = SemesterMapper.ToEntity(viewModel);
            UnitOfWork.SemesterRepository.Add(entity);
        }

        public void Edit(SemesterViewModel viewModel)
        {
            var entity = SemesterMapper.ToEntity(viewModel);
            UnitOfWork.SemesterRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            var gradeStudent = UnitOfWork.GradeStudentRepository.FindBy(p => p.SemesterId == id).ToList();
            foreach (var item in gradeStudent)
            {
                UnitOfWork.GradeStudentRepository.Delete(p => p.SemesterId == id);
            }

            UnitOfWork.SemesterRepository.Delete(p => p.SemesterId == id);
        }
    }
}
