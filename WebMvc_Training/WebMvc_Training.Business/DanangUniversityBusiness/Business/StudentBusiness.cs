﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class StudentBusiness : BaseBusiness, IStudentBusiness
    {
        public List<StudentViewModel> GetAll()
        {
            var students = UnitOfWork.StudentRepository.GetAll().ToList();
            return StudentMapper.ToViewModel(students);
        }

        public List<StudentViewModel> FindBy(int id)
        {
            var students = UnitOfWork.StudentRepository.FindBy(p => p.ClassId == id).ToList();
            return StudentMapper.ToViewModel(students);
        }
        
        public StudentViewModel GetSingle(int id)
        {
            var student = UnitOfWork.StudentRepository.FindBy(p => p.StudentId == id).SingleOrDefault();
            return StudentMapper.ToViewModel(student);
        }

        public void Add(StudentViewModel viewModel)
        {
            var entity = StudentMapper.ToEntity(viewModel);
            UnitOfWork.StudentRepository.Add(entity);
        }

        public void Edit(StudentViewModel viewModel)
        {
            var entity = StudentMapper.ToEntity(viewModel);
            UnitOfWork.StudentRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            var gradeStudents = UnitOfWork.GradeStudentRepository.FindBy(p => p.StudentId == id).ToList();
            foreach (var item in gradeStudents)
            {
                UnitOfWork.GradeStudentRepository.Delete(p => p.StudentId == item.StudentId);
            }

            UnitOfWork.StudentRepository.Delete(p => p.StudentId == id);
        }
    }
}
