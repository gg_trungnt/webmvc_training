﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class SubjectBusiness : BaseBusiness, ISubjectBusiness
    {
        public List<SubjectViewModel> GetAll()
        {
            var subjects = UnitOfWork.SubjectRepository.GetAll().ToList();
            return SubjectMapper.ToViewModel(subjects);
        }

        public List<SubjectViewModel> FindBy(int id)
        {
            var subjects = UnitOfWork.SubjectRepository.FindBy(p => p.FacultyId == id).OrderBy(p => p.SubjectName).ToList();
            return SubjectMapper.ToViewModel(subjects);
        }
        
        public SubjectViewModel GetSingle(int id)
        {
            var entity = UnitOfWork.SubjectRepository.FindBy(p => p.SubjectId == id).SingleOrDefault();
            return SubjectMapper.ToViewModel(entity);
        }

        public void Add(SubjectViewModel viewModel)
        {
            var entity = SubjectMapper.ToEntity(viewModel);
            UnitOfWork.SubjectRepository.Add(entity);
        }

        public void Edit(SubjectViewModel viewModel)
        {
            var entity = SubjectMapper.ToEntity(viewModel);
            UnitOfWork.SubjectRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            var gradeStudents = UnitOfWork.GradeStudentRepository.FindBy(p => p.SubjectId == id).ToList();
            foreach (var item in gradeStudents)
            {
                UnitOfWork.GradeStudentRepository.Delete(p => p.SubjectId == item.SubjectId);
            }

            UnitOfWork.SubjectRepository.Delete(p => p.SubjectId == id);
        }
    }
}
