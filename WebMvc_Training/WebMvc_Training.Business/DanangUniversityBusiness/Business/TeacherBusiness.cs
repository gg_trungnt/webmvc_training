﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.Mapper;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.Business
{
    public class TeacherBusiness : BaseBusiness, ITeacherBusiness
    {
        public List<TeacherViewModel> GetAll()
        {
            var teachers = UnitOfWork.TeacherRepository.GetAll().ToList();
            return TeacherMapper.ToViewModel(teachers);
        }

        public List<TeacherViewModel> FindBy(int id)
        {
            var teachers = UnitOfWork.TeacherRepository.FindBy(p => p.FacultyId == id).ToList();
            return TeacherMapper.ToViewModel(teachers);
        }
        
        public TeacherViewModel GetSingle(int id)
        {
            var entity = UnitOfWork.TeacherRepository.FindBy(p => p.TeacherId == id).SingleOrDefault();
            return TeacherMapper.ToViewModel(entity);
        }

        public void Add(TeacherViewModel viewModel)
        {
            var entity = TeacherMapper.ToEntity(viewModel);
            UnitOfWork.TeacherRepository.Add(entity);
        }

        public void Edit(TeacherViewModel viewModel)
        {
            var entity = TeacherMapper.ToEntity(viewModel);
            UnitOfWork.TeacherRepository.Edit(entity);
        }

        public void Delete(int id)
        {
            var gradeStudents = UnitOfWork.GradeStudentRepository.FindBy(p => p.TeacherId == id).ToList();
            foreach (var item in gradeStudents)
            {
                UnitOfWork.GradeStudentRepository.Delete(p => p.TeacherId == item.TeacherId);
            }

            UnitOfWork.TeacherRepository.Delete(p => p.TeacherId == id);
        }
    }
}
