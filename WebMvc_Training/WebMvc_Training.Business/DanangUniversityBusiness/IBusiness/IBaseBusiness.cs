﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Business.DanangUniversityBusiness.IBusiness
{
    public interface IBaseBusiness<T> where T : class
    {
        List<T> GetAll();
        List<T> FindBy(int id);
        T GetSingle(int id);
        void Add(T viewModel);
        void Edit(T viewModel);
        void Delete(int id);
    }
}
