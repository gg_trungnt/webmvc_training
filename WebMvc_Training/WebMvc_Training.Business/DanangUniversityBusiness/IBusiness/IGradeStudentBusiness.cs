﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.Business.DanangUniversityBusiness.IBusiness
{
    public interface IGradeStudentBusiness : IBaseBusiness<GradeStudentViewModel>
    {
        List<GradeStudentViewModel> FindBy(int studentId, int semesterId);
    }
}
