﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class ClassMapper
    {
        public static ClassViewModel ToViewModel(Class entity)
        {
            if (entity == null) return null;
            return new ClassViewModel()
            {
                ClassId = entity.ClassId,
                FacultyId = entity.FacultyId,
                FacultyName = entity.Faculty.FacultyName,
                ClassName = entity.ClassName,
                Description = entity.Description
            };
        }

        public static Class ToEntity(ClassViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new Class()
            {
                ClassId = viewModel.ClassId,
                FacultyId = viewModel.FacultyId,
                ClassName = viewModel.ClassName,
                Description = viewModel.Description
            };
        }

        public static List<ClassViewModel> ToViewModel(List<Class> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<Class> ToEntity(List<ClassViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
