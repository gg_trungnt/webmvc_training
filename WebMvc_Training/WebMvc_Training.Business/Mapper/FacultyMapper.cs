﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class FacultyMapper
    {
        public static FacultyViewModel ToViewModel(Faculty entity)
        {
            if (entity == null) return null;
            return new FacultyViewModel()
            {
                FacultyId = entity.FacultyId,
                FacultyName = entity.FacultyName,
                Description = entity.Description
            };
        } 

        public static Faculty ToEntity(FacultyViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new Faculty()
            {
                FacultyId = viewModel.FacultyId,
                FacultyName = viewModel.FacultyName,
                Description = viewModel.Description
            };
        }

        public static List<FacultyViewModel> ToViewModel(List<Faculty> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<Faculty> ToEntity(List<FacultyViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
