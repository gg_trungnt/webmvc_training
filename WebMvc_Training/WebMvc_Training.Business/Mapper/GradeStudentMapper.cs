﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class GradeStudentMapper
    {
        public static GradeStudentViewModel ToViewModel(GradeStudent entity)
        {
            if (entity == null) return null;
            return new GradeStudentViewModel()
            {
                StudentId = entity.StudentId,
                TeacherId = entity.TeacherId,
                SubjectId = entity.SubjectId,
                SemesterId = entity.SemesterId,
                SubjectName = entity.Subject.SubjectName,
                Point = (float) entity.Point
            };
        }

        public static GradeStudent ToEntity(GradeStudentViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new GradeStudent()
            {
                StudentId = viewModel.StudentId,
                TeacherId = viewModel.TeacherId,
                SubjectId = viewModel.SubjectId,
                SemesterId = viewModel.SemesterId,
                Point = viewModel.Point
            };
        }

        public static List<GradeStudentViewModel> ToViewModel(List<GradeStudent> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<GradeStudent> ToEntity(List<GradeStudentViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
