﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class SemesterMapper
    {
        public static SemesterViewModel ToViewModel(Semester entity)
        {
            if (entity == null) return null;
            return new SemesterViewModel()
            {
                SemesterId = entity.SemesterId,
                SemesterName = entity.SemesterName
            };
        }

        public static Semester ToEntity(SemesterViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new Semester()
            {
                SemesterId = viewModel.SemesterId,
                SemesterName = viewModel.SemesterName
            };
        }

        public static List<SemesterViewModel> ToViewModel(List<Semester> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<Semester> ToEntity(List<SemesterViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
