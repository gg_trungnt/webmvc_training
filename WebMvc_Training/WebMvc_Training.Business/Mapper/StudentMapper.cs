﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class StudentMapper
    {
        public static StudentViewModel ToViewModel(Student entity)
        {
            if (entity == null) return null;
            return new StudentViewModel()
            {
                StudentId = entity.StudentId,
                ClassId = entity.ClassId,
                ClassName = entity.Class.ClassName,
                StudentName = entity.StudentName,
                Birthday = entity.Birthday.Value,
                Address = entity.Address,
                Phone = entity.Phone,
                Note = entity.Note
            };
        }

        public static Student ToEntity(StudentViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new Student()
            {
                StudentId = viewModel.StudentId,
                ClassId = viewModel.ClassId,
                StudentName = viewModel.StudentName,
                Birthday = viewModel.Birthday,
                Address = viewModel.Address,
                Phone = viewModel.Phone,
                Note = viewModel.Note
            };
        }

        public static List<StudentViewModel> ToViewModel(List<Student> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<Student> ToEntity(List<StudentViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
