﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class SubjectMapper
    {
        public static SubjectViewModel ToViewModel(Subject entity)
        {
            if (entity == null) return null;
            return new SubjectViewModel()
            {
                SubjectId = entity.SubjectId,
                FacultyId = entity.FacultyId,
                FacultyName = entity.Faculty.FacultyName,
                SubjectName = entity.SubjectName,
                Description = entity.Description,
                Document1 = entity.Document1,
                Document2 = entity.Document2,
                Document3 = entity.Document3
            };
        }

        public static Subject ToEntity(SubjectViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new Subject()
            {
                SubjectId = viewModel.SubjectId,
                FacultyId = viewModel.FacultyId,
                SubjectName = viewModel.SubjectName,
                Description = viewModel.Description,
                Document1 = viewModel.Document1,
                Document2 = viewModel.Document2,
                Document3 = viewModel.Document3
            };
        }

        public static List<SubjectViewModel> ToViewModel(List<Subject> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<Subject> ToEntity(List<SubjectViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
