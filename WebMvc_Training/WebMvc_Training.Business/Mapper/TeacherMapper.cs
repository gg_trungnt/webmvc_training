﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Business.ViewModel;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Business.Mapper
{
    public static class TeacherMapper
    {
        public static TeacherViewModel ToViewModel(Teacher entity)
        {
            if (entity == null) return null;
            return new TeacherViewModel()
            {
                TeacherId = entity.TeacherId,
                FacultyId = entity.FacultyId,
                FacultyName = entity.Faculty.FacultyName,
                TeacherName = entity.TeacherName,
                Address = entity.Address,
                Phone = entity.Phone,
                Note = entity.Note
            };
        }

        public static Teacher ToEntity(TeacherViewModel viewModel)
        {
            if (viewModel == null) return null;
            return new Teacher()
            {
                TeacherId = viewModel.TeacherId,
                FacultyId = viewModel.FacultyId,
                TeacherName = viewModel.TeacherName,
                Address = viewModel.Address,
                Phone = viewModel.Phone,
                Note = viewModel.Note
            };
        }

        public static List<TeacherViewModel> ToViewModel(List<Teacher> entities)
        {
            return entities.Select(p => ToViewModel(p)).ToList();
        }

        public static List<Teacher> ToEntity(List<TeacherViewModel> viewModels)
        {
            return viewModels.Select(p => ToEntity(p)).ToList();
        }
    }
}
