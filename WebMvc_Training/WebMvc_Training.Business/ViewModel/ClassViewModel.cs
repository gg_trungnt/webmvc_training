﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Business.ViewModel
{
    public class ClassViewModel
    {
        public int ClassId { get; set; }
        public int FacultyId { get; set; }
        public string FacultyName { get; set; }
        public string ClassName { get; set; }
        public string Description { get; set; }
    }
}
