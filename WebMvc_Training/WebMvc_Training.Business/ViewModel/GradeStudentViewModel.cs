﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Business.ViewModel
{
    public class GradeStudentViewModel
    {
        public int StudentId { get; set; }
        public int TeacherId { get; set; }
        public int SubjectId { get; set; }
        public int SemesterId { get; set; }
        public string SubjectName { get; set; }
        public float Point { get; set; }
    }
}
