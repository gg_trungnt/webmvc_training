﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Business.ViewModel
{
    public class SemesterViewModel
    {
        public int SemesterId { get; set; }
        public string SemesterName { get; set; }
    }
}
