﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Business.ViewModel
{
    public class SubjectViewModel
    {
        public int SubjectId { get; set; }
        public int FacultyId { get; set; }
        public string FacultyName { get; set; }
        public string SubjectName { get; set; }
        public string Description { get; set; }
        public string Document1 { get; set; }
        public string Document2 { get; set; }
        public string Document3 { get; set; }
    }
}
