﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Business.ViewModel
{
    public class TeacherViewModel
    {
        public int TeacherId { get; set; }
        public int FacultyId { get; set; }
        public string FacultyName { get; set; }
        public string TeacherName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
    }
}
