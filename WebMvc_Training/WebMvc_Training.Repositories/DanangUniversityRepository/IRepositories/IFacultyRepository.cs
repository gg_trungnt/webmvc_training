﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Repositories.DanangUniversityRepository.IRepositories
{
    public interface IFacultyRepository : IGenericRepository<Faculty>
    {
    }
}
