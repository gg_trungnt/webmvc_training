﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Repositories.DanangUniversityRepository.IRepositories;
using WebMvc_Training.Repositories.UnitOfWork;

namespace WebMvc_Training.Repositories.DanangUniversityRepository.Repositories
{
    public class GenericRepository<C, T> : IGenericRepository<T> where T : class where C : DbContext, new()
    {
        //protected C DbContext = new C();

        protected IUnitOfWork<C> _UnitOfWork;

        public GenericRepository(IUnitOfWork<C> unitOfWork)
        {
            _UnitOfWork = unitOfWork;
        }

        public IQueryable<T> GetAll()
        {
            return _UnitOfWork.DbContext.Set<T>().AsQueryable();
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _UnitOfWork.DbContext.Set<T>().Where(predicate).AsQueryable();
        }

        public void Add(T entity)
        {
            _UnitOfWork.DbContext.Set<T>().Add(entity);
            _UnitOfWork.DbContext.SaveChanges();
        }

        public void Edit(T entity)
        {
            _UnitOfWork.DbContext.Entry(entity).State = EntityState.Modified;
            _UnitOfWork.DbContext.SaveChanges();
        }

        public void Delete(Expression<Func<T, bool>> predicate)
        {
            var entity = _UnitOfWork.DbContext.Set<T>().Where(predicate).SingleOrDefault();
            _UnitOfWork.DbContext.Set<T>().Remove(entity);
            _UnitOfWork.DbContext.SaveChanges();
        }
    }
}
