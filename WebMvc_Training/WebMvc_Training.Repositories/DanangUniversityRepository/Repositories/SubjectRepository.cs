﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Entities;
using WebMvc_Training.Repositories.DanangUniversityRepository.IRepositories;
using WebMvc_Training.Repositories.UnitOfWork;

namespace WebMvc_Training.Repositories.DanangUniversityRepository.Repositories
{
    public class SubjectRepository : GenericRepository<DanangUniversityDbContext, Subject>, ISubjectRepository
    {
        public SubjectRepository(IUnitOfWork<DanangUniversityDbContext> unitOfWork) : base(unitOfWork)
        {
        }
    }
}
