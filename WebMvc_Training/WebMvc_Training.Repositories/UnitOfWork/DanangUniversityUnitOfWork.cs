﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Entities;
using WebMvc_Training.Repositories.DanangUniversityRepository.Repositories;

namespace WebMvc_Training.Repositories.UnitOfWork
{
    public class DanangUniversityUnitOfWork : UnitOfWork<DanangUniversityDbContext>
    {
        public DanangUniversityUnitOfWork(DanangUniversityDbContext context) : base(context)
        {
        }

        public StudentRepository StudentRepository
        {
            get { return new StudentRepository(this); }
        }

        public ClassRepository ClassRepository
        {
            get { return new ClassRepository(this); }
        }

        public FacultyRepository FacultyRepository
        {
            get { return new FacultyRepository(this); }
        }

        public SubjectRepository SubjectRepository
        {
            get { return new SubjectRepository(this); }
        }

        public SemesterRepository SemesterRepository
        {
            get { return new SemesterRepository(this); }
        }

        public GradeStudentRepository GradeStudentRepository
        {
            get { return new GradeStudentRepository(this); }
        }

        public TeacherRepository TeacherRepository
        {
            get { return new TeacherRepository(this); }
        }
    }
}
