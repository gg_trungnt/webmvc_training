﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMvc_Training.Repositories.UnitOfWork
{
    public interface IUnitOfWork<C> where C : DbContext
    {
        C DbContext { get; }
        void Commit();
    }
}
