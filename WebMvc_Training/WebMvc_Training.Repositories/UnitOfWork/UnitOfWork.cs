﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebMvc_Training.Entities;

namespace WebMvc_Training.Repositories.UnitOfWork
{
    public class UnitOfWork<C> : IUnitOfWork<C> where C : DbContext
    {
        public C DbContext { get; set; }

        public UnitOfWork(C context)
        {
            DbContext = context;
        }

        public void Commit()
        {
            DbContext.SaveChanges();
        }
    }
}
