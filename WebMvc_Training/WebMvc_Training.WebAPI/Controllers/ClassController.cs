﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class ClassController : ApiController
    {
        private IClassBusiness _Business;

        public ClassController(IClassBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetAll()
        {
            var classes = _Business.GetAll();

            if (classes == null) return NotFound();

            return Ok(classes);
        }

        public IHttpActionResult GetClasses(int facultyId)
        {
            if (facultyId <= 0) return BadRequest("Not a valid facultyId");

            var classes = _Business.FindBy(facultyId);

            if (classes == null) return NotFound();

            return Ok(classes);
        }

        public IHttpActionResult GetSingle(int facultyId)
        {
            if (facultyId <= 0) return BadRequest("Not a valid facultyId");

            var singleClass = _Business.GetSingle(facultyId);

            if (singleClass == null) return NotFound();

            return Ok(singleClass);
        }

        public IHttpActionResult PostSingle(ClassViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);
            return Ok();
        }

        public IHttpActionResult PutSingle(ClassViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);
            return Ok();
        }

        public IHttpActionResult DeleteSingle(int classId)
        {
            if (classId <= 0) return BadRequest("Not a valid classId");

            _Business.Delete(classId);
            return Ok();
        }
    }
}
