﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class FacultyController : ApiController
    {
        private IFacultyBusiness _Business;

        public FacultyController(IFacultyBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetAll()
        {
            var faculties = _Business.GetAll();

            if (faculties == null) return NotFound();

            return Ok(faculties);
        }

        public IHttpActionResult GetSingle(int facultyId)
        {
            if (facultyId <= 0) return BadRequest("Not a valid facultyId");

            var faculty = _Business.GetSingle(facultyId);

            if (faculty == null) return NotFound();

            return Ok(faculty);
        }

        public IHttpActionResult PostSingle(FacultyViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);

            return Ok();
        }

        public IHttpActionResult PutSingle(FacultyViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);

            return Ok();
        }

        public IHttpActionResult DeleteSingle(int facultyId)
        {
            if (facultyId <= 0) return BadRequest("Not a valid facultyId");

            _Business.Delete(facultyId);

            return Ok();
        }

    }
}
