﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class GradeStudentController : ApiController
    {
        private IGradeStudentBusiness _Business;

        public GradeStudentController(IGradeStudentBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetGradeStudents(int studentId, int semesterId)
        {
            if (studentId <= 0 || semesterId <= 0) return BadRequest("Not a valid id");

            var gradeStudents = _Business.FindBy(studentId, semesterId);

            if (gradeStudents == null) return NotFound();

            return Ok(gradeStudents);
        }

        public IHttpActionResult PostSingle(GradeStudentViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);

            return Ok();
        }

        public IHttpActionResult PutSingle(GradeStudentViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);

            return Ok();
        }
    }
}
