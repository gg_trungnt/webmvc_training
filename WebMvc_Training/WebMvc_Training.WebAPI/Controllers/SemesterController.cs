﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class SemesterController : ApiController
    {
        private ISemesterBusiness _Business;

        public SemesterController(ISemesterBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetAll()
        {
            var semesters = _Business.GetAll();

            if (semesters == null) return NotFound();

            return Ok(semesters);
        }

        public IHttpActionResult GetSingle(int semesterId)
        {
            if (semesterId <= 0) return BadRequest("Not a valid semesterId");

            var semester = _Business.GetSingle(semesterId);

            if (semester == null) return NotFound();

            return Ok(semester);
        }
        
        public IHttpActionResult PostSingle(SemesterViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);

            return Ok();
        }

        public IHttpActionResult PutSingle(SemesterViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);

            return Ok();
        }

        public IHttpActionResult DeleteSingle(int semesterId)
        {
            if (semesterId <= 0) return BadRequest("Not a valid semesterId");

            _Business.Delete(semesterId);

            return Ok();
        }
    }
}
