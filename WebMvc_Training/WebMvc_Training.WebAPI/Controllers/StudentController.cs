﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class StudentController : ApiController
    {
        private IStudentBusiness _Business;

        public StudentController(IStudentBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetAll()
        {
            var students = _Business.GetAll();

            if (students == null) return NotFound();

            return Ok(students);
        }

        public IHttpActionResult GetStudents(int classId)
        {
            if (classId <= 0) return BadRequest("Not a valid classId");

            var classes = _Business.FindBy(classId);

            if (classes == null) return NotFound();

            return Ok(classes);
        }

        public IHttpActionResult GetSingle(int studentId)
        {
            if (studentId <= 0) return BadRequest("Not a valid studentId");

            var student = _Business.GetSingle(studentId);

            if (student == null) return NotFound();

            return Ok(student);
        }

        public IHttpActionResult PostSingle(StudentViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);
            return Ok();
        }

        public IHttpActionResult Put(StudentViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);
            return Ok();
        }

        public IHttpActionResult Delete(int studentId)
        {
            if (studentId <= 0) return BadRequest("Not a valid studentId");

            _Business.Delete(studentId);
            return Ok();
        }
    }
}
