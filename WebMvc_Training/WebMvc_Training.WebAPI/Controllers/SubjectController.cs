﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class SubjectController : ApiController
    {
        private ISubjectBusiness _Business;

        public SubjectController(ISubjectBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetAll()
        {
            var subjects = _Business.GetAll();

            if (subjects == null) return NotFound();

            return Ok(subjects);
        }

        public IHttpActionResult GetSingle(int subjectId)
        {
            var subject = _Business.GetSingle(subjectId);

            if (subject == null) return NotFound();

            return Ok(subject);
        }

        public IHttpActionResult GetSubjects(int facultyId)
        {
            if (facultyId <= 0) return BadRequest("Not a valid facultyId");

            var subjects = _Business.FindBy(facultyId);

            if (subjects == null) return NotFound();

            return Ok(subjects);
        }

        public IHttpActionResult PostSingle(SubjectViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);

            return Ok();
        }

        public IHttpActionResult PutSingle(SubjectViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);

            return Ok();
        }

        public IHttpActionResult DeleteSingle(int subjectId)
        {
            if (subjectId <= 0) return BadRequest("Not a valid data");

            _Business.Delete(subjectId);

            return Ok();
        }
    }
}
