﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebAPI.Controllers
{
    public class TeacherController : ApiController
    {
        private ITeacherBusiness _Business;

        public TeacherController(ITeacherBusiness business)
        {
            _Business = business;
        }

        public IHttpActionResult GetAll()
        {
            var teachers = _Business.GetAll();

            if (teachers == null) return NotFound();

            return Ok(teachers);
        }

        public IHttpActionResult GetSingle(int teacherId)
        {
            if (teacherId <= 0) return BadRequest("Not a valid teacherId");

            var teacher = _Business.GetSingle(teacherId);

            if (teacher == null) return NotFound();

            return Ok(teacher);
        }

        public IHttpActionResult GetTeachers(int facultyId)
        {
            if (facultyId <= 0) return BadRequest("Not a valid facultyId");

            var teacher = _Business.FindBy(facultyId);

            if (teacher == null) return NotFound();

            return Ok(teacher);
        }

        public IHttpActionResult PostSingle(TeacherViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Add(viewModel);

            return Ok();
        }

        public IHttpActionResult PutSingle(TeacherViewModel viewModel)
        {
            if (!ModelState.IsValid) return BadRequest("Not a valid data");

            _Business.Edit(viewModel);

            return Ok();
        }

        public IHttpActionResult DeleteSingle(int teacherId)
        {
            if (teacherId <= 0) return BadRequest("Not a valid data");

            _Business.Delete(teacherId);

            return Ok();
        }
    }
}
