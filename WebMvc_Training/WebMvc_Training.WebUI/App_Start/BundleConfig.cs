﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebMvc_Training.WebUI.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/script")
                .Include("~/Scripts/Login.js"));

            bundles.Add(new ScriptBundle("~/Scripts/bundle-script")
                .Include("~/Scripts/Login.js", "~/Scripts/script.js"));
            
            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/Login.css"));

            bundles.Add(new StyleBundle("~/Content/bundle-style")
                .Include("~/Content/style.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}