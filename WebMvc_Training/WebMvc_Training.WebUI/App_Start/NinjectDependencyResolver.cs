﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.Business;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;

namespace WebMvc_Training.WebUI.App_Start
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IAccountBusiness>().To<AccountBusiness>();
            kernel.Bind<IFacultyBusiness>().To<FacultyBusiness>();
            kernel.Bind<IClassBusiness>().To<ClassBusiness>();
            kernel.Bind<IStudentBusiness>().To<StudentBusiness>();
            kernel.Bind<ITeacherBusiness>().To<TeacherBusiness>();
            kernel.Bind<ISubjectBusiness>().To<SubjectBusiness>();
            kernel.Bind<IGradeStudentBusiness>().To<GradeStudentBusiness>();
            kernel.Bind<ISemesterBusiness>().To<SemesterBusiness>();
        }
    }
}