﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebUI.Controllers
{
    public class AccountController : Controller
    {
        private IAccountBusiness _Business;

        public AccountController(IAccountBusiness business)
        {
            _Business = business;
        }

        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(AccountViewModel viewModel)
        {
            var result = _Business.VerifyAccount(viewModel);

            string responseData = string.Empty;

            if (result)
            {
                responseData = "Success";
                Session["Username"] = "admin";
            }

            return Json(new { Message = responseData });
        }

        public ActionResult Logout()
        {
            Session["Username"] = null;
            var redirectUrl = new UrlHelper(Request.RequestContext).Action("Login", "Account");

            return Json(new { Message = redirectUrl });
        }
    }
}