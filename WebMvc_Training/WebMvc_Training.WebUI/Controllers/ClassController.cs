﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebUI.Controllers
{
    public class ClassController : BaseController
    {
        private IClassBusiness _ClassBusiness;
        private IFacultyBusiness _FacultyBusiness;

        public ClassController(IClassBusiness classBusiness, IFacultyBusiness facultyBusienss)
        {
            _ClassBusiness = classBusiness;
            _FacultyBusiness = facultyBusienss;
        }

        // GET: Class
        public ActionResult List(int id = 0)
        {
            ViewData["Faculties"] = _FacultyBusiness.GetAll();
            ViewData["FacultyId"] = id;

            var model = _ClassBusiness.FindBy(id);
            return PartialView(model);
        }

        public ActionResult Detail(int id)
        {
            var model = _ClassBusiness.GetSingle(id);
            return PartialView(model);
        }

        public ActionResult Create()
        {
            ViewData["Faculties"] = _FacultyBusiness.GetAll();
            return PartialView();
        }

        public ActionResult CreatePost(ClassViewModel viewModel)
        {
            _ClassBusiness.Add(viewModel);
            return Json(new { Message = "Success"});
        }

        public ActionResult Edit(int id)
        {
            var model = _ClassBusiness.GetSingle(id);
            return PartialView(model);
        }

        public ActionResult EditPost(ClassViewModel viewModel)
        {
            _ClassBusiness.Edit(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Delete(int id)
        {
            _ClassBusiness.Delete(id);
            return Json(new { Message = "Success" });
        }
    }
}