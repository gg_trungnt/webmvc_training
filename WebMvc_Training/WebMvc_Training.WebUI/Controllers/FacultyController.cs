﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebUI.Controllers
{
    public class FacultyController : BaseController
    {
        private IFacultyBusiness _Business;

        public FacultyController(IFacultyBusiness business)
        {
            _Business = business;
        }

        // GET: Faculty
        public ActionResult List()
        {
            var model = _Business.GetAll();
            return View(model);
        }

        public ActionResult Detail(int id)
        {
            var model = _Business.GetSingle(id);
            return PartialView(model);
        }

        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreatePost(FacultyViewModel viewModel)
        {
            _Business.Add(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Edit(int id)
        {
            var model = _Business.GetSingle(id);
            return PartialView(model);
        }

        public ActionResult EditPost(FacultyViewModel viewModel)
        {
            _Business.Edit(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Delete(int id)
        {
            _Business.Delete(id);
            return Json(new { Message = "Success" });
        }
    }
}