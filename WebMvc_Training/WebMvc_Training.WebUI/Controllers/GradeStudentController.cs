﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebUI.Controllers
{
    public class GradeStudentController : BaseController
    {
        private IGradeStudentBusiness _Business;

        public GradeStudentController(IGradeStudentBusiness business)
        {
            _Business = business;
        }
        // GET: GradeStudent
        public ActionResult List(int studentId, int semesterId = 1)
        {
            var model = _Business.FindBy(studentId, semesterId);
            return PartialView(model);
        }

        public ActionResult ListPlus(int studentId, int semesterId = 1)
        {
            var model = _Business.FindBy(studentId, semesterId);
            ViewData["StudentId"] = studentId;
            ViewData["SemesterId"] = semesterId;

            return PartialView(model);
        }

        public ActionResult Edit(int studentId, int semesterId, int teacherId, int subjectId, float point)
        {
            var viewModel = new GradeStudentViewModel()
            {
                StudentId = studentId,
                TeacherId = teacherId,
                SubjectId = subjectId,
                SemesterId = semesterId,
                Point = point
            };

            _Business.Edit(viewModel);

            return Json(new { Message = "Success" });
        }
    }
}