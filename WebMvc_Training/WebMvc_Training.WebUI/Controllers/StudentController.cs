﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebUI.Controllers
{
    public class StudentController : BaseController
    {
        private IStudentBusiness _Business;
        private IClassBusiness _ClassBusiness;
        private IFacultyBusiness _FacultyBusiness;
        private ISemesterBusiness _SemesterBusiness;

        public StudentController(IStudentBusiness business, IClassBusiness classBusiness, IFacultyBusiness facultyBusiness, ISemesterBusiness semesterBusiness)
        {
            _Business = business;
            _ClassBusiness = classBusiness;
            _FacultyBusiness = facultyBusiness;
            _SemesterBusiness = semesterBusiness;
        }

        // GET: Student
        public ActionResult List(int facultyId = 0, int classId = 0)
        {
            ViewData["Faculties"] = _FacultyBusiness.GetAll();
            ViewData["FacultyId"] = facultyId;
            ViewData["Classes"] = _ClassBusiness.FindBy(facultyId);
            ViewData["ClassId"] = classId;

            var model = _Business.FindBy(classId);

            return PartialView(model);
        }

        public ActionResult Detail(int id)
        {
            var model = _Business.GetSingle(id);

            var singleClass = _ClassBusiness.GetSingle(model.ClassId);
            ViewData["ClassName"] = singleClass.ClassName;
            ViewData["FacultyName"] = singleClass.FacultyName;
            ViewData["FacultyId"] = singleClass.FacultyId;
            ViewData["Semesters"] = _SemesterBusiness.GetAll();

            return PartialView(model);
        }

        public ActionResult Create(int id = 0)
        {
            ViewData["Faculties"] = _FacultyBusiness.GetAll();
            ViewData["FacultyId"] = id;
            ViewData["Classes"] = _ClassBusiness.FindBy(id);


            return PartialView();
        }

        public ActionResult CreatePost(StudentViewModel viewModel)
        {
            _Business.Add(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Edit(int id)
        {
            var model = _Business.GetSingle(id);

            var singleClass = _ClassBusiness.GetSingle(model.ClassId);
            ViewData["ClassName"] = singleClass.ClassName;
            ViewData["FacultyName"] = singleClass.FacultyName;
            ViewData["FacultyId"] = singleClass.FacultyId;
            ViewData["Semesters"] = _SemesterBusiness.GetAll();

            return PartialView(model);
        }

        public ActionResult EditPost(StudentViewModel viewModel)
        {
            _Business.Edit(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Delete(int id)
        {
            _Business.Delete(id);
            return Json(new { Message = "Success" });
        }
        
    }
}