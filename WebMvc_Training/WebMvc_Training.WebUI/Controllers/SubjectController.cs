﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMvc_Training.Business.DanangUniversityBusiness.IBusiness;
using WebMvc_Training.Business.ViewModel;

namespace WebMvc_Training.WebUI.Controllers
{
    public class SubjectController : BaseController
    {
        private ISubjectBusiness _Business;
        private IFacultyBusiness _FacultyBusiness;

        public SubjectController(ISubjectBusiness business, IFacultyBusiness facultyBusiness)
        {
            _Business = business;
            _FacultyBusiness = facultyBusiness;
        }

        // GET: Subject
        public ActionResult List(int id = 0)
        {
            ViewData["Faculties"] = _FacultyBusiness.GetAll();
            ViewData["FacultyId"] = id;

            var model = _Business.FindBy(id);
            return PartialView(model);
        }

        public ActionResult Detail(int id = 0)
        {
            var model = _Business.GetSingle(id);
            return PartialView(model);
        }

        public ActionResult Create()
        {
            ViewData["Faculties"] = _FacultyBusiness.GetAll();
            return PartialView();
        }

        public ActionResult CreatePost(SubjectViewModel viewModel)
        {
            _Business.Add(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Edit(int id)
        {
            var model = _Business.GetSingle(id);
            return PartialView(model);
        }

        public ActionResult EditPost(SubjectViewModel viewModel)
        {
            _Business.Edit(viewModel);
            return Json(new { Message = "Success" });
        }

        public ActionResult Delete(int id)
        {
            _Business.Delete(id);
            return Json(new { Message = "Success"});
        }
    }
}