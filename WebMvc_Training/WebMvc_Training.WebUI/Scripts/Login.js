﻿var Login = {
    Login: function () {
        var username = $('#username').val();
        var password = $('#password').val();

        if (username != '' && password != '') {
            $.ajax({
                url: "/Account/Login",
                type: "POST",
                data: $('#form-login').serialize(),
                success: function (data) {
                    if (data.Message == 'Success') {
                        window.location.href = "/Trang-chu";
                    } else {
                        $('#error-message').text("Your account is not correct!");
                    }
                }
            });
        } else {
            $('#error-message').text('You must enter all of the textbox');
        }
    },

    Logout: function () {
        $.ajax({
            url: "/Account/Logout",
            type: "POST",
            success: function (data) {
                window.location.href = data.Message;
            }
        });  
    }
}