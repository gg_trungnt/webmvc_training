//function menuTab(url) {
//    $.get(url).done(function (data) {
//        $("#content-page").html(data);
//    });
//}

var Load = {
    LoadPage: function (url) {
        //$.get(url).done(function (data) {
        //    $('#context-page').html(data);
        //});
        $('.menu-nav').find('li').removeClass('current');
        $(this).parent().addClass('current');

        $('#content-page').load(url);
    },

    LoadPage: function (url, id) {
        $('#content-page').load(url, { 'id' : id });

        //$.ajax({
        //    url: '/Faculty/Detail',
        //    type: 'POST',
        //    success: function (data) {
        //        $('#content-page').html(data);
        //    }
        //});
    },

    Action: function (urlAction, urlBack) {
        var status = $('#status').val();

        var isValid = true;

        $('.required').each(function (index, item) {
            if ($(item).val() == '') isValid = false;
        });

        if (isValid) {
            $.ajax({
                url: urlAction,
                type: 'POST',
                data: $('.formAction').serialize(),
                success: function (data) {
                    if (data.Message == 'Success' && status == 0) {
                        window.location.href = '/Faculty/List';
                    } else if (data.Message == 'Success') {
                        Load.LoadPage(urlBack);
                    }
                }
            });
        } else {
            $('#error').text('Your must enter the required field!');
        }
    },

    DeleteAction: function (urlAction, urlBack, id, statusCode) {
        $.ajax({
            url: urlAction,
            type: 'POST',
            data: {'id' : id},
            success: function (data) {
                if (data.Message == 'Success' && statusCode == 0) {
                    window.location.href = '/Faculty/List';
                } else if (data.Message == 'Success') {
                    Load.LoadPage(urlBack);
                }
            }
        });    
    }
}

